A repo for me to use to store various ComputerCraft Lua scripts
===============================================================

See the wiki for more info on each script and how to use them

Scripts avaliable in this repo:
* staircase.lua -- Mining Turtle script, digs a staircase downwards; by cookie4270
* ComputerLockCode.lua -- Computer script, passcode lock to open a door or anything redstone controlled; by minerking
