-- Define local variable 'times'
local times
-- Ask how many layers to dig down
term.write("How many layers to dig? ")
-- Set local variable times to input
times = read()

-- For loop to make the turtle dig for 'times' layers
for v=0,times do
    turtle.digDown()
    turtle.Down()
    turtle.dig()
    turtle.moveForward()
-- Anti-gravel loop; if cannot move forward because of gravel, break the gravel
        while not turtle.moveForward() do
            turtle.dig()
        end
    turtle.digUp()
    sleep(1)
end
-- By this point the turtle should have dug 'times' layers

print("Successfully dug ", times, "layers!")
print("Staircase created!")
print("Is it not working? File an issue at https://github.com/cookie4270/CCMiningTurtle")

-- Script done!
